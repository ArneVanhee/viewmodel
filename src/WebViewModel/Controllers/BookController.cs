﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using WebViewModel.Models;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace WebViewModel.Controllers
{
    public class BookController : Controller
    {
        // GET: /Book/
        public IActionResult Index()
        {
            LibraryData libData = new LibraryData();
            List<Book> books = libData.GetAllBooks();

            return View(books);
        }

        // GET: /Book/1
        public IActionResult Details(int id)
        {
            LibraryData libData = new LibraryData();
            Book book = libData.GetBookById(id);

            return View(book);
        }
    }
}
